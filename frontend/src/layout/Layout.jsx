import React from 'react'

export default function Layout({ children, className }) {
    return (
        <div className={`min-h-screen container mx-auto px-4 ${className}`}>
            {children}
        </div>
    )
}
