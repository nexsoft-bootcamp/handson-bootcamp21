import { CSS } from "@dnd-kit/utilities";
import { useSortable } from "@dnd-kit/sortable";

const Card = ({ id, title, children }) => {
    const { attributes, listeners, setNodeRef, transform } = useSortable({
        id: id
    });

    const style = {
        transform: CSS.Transform.toString(transform)
    };

    return (
        <div ref={setNodeRef} {...attributes} {...listeners} style={style}>
            <div id={id} className="card w-96 bg-base-100 shadow-xl rounded-sm">
                <div className="card-body" style={{ backgroundColor: '#D7E4C0' }}>
                    <h2 className="card-title text-sm font-semibold">{title}</h2>
                    <p className="text-sm">
                        {children}
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Card;
