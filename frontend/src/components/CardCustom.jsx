import React from 'react'
import ButtonCustom from './ButtonCustom'

export default function CardCustom({ title, children, onClick }) {
    return (
        <div className="card w-96 bg-base-100 shadow-xl rounded-sm">
            <div className="card-body">
                <h2 className="card-title text-sm font-semibold">{title}</h2>
                {children}
                {
                    onClick ? <div className="card-actions justify-end">
                        <ButtonCustom onCLick={onClick} name='Create' />
                    </div> : <></>
                }

            </div>
        </div>
    )
}
