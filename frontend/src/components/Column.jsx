import { FC } from "react";
import { SortableContext, rectSortingStrategy } from "@dnd-kit/sortable";
import { useDroppable } from "@dnd-kit/core";
import Card from "./Card";

const Column = ({ id, title, cards }) => {
    const { setNodeRef } = useDroppable({ id: id });

    return (
        <SortableContext id={id} items={cards} strategy={rectSortingStrategy}>
            <div ref={setNodeRef}>
                <h1 className="mb-4 text-white" style={{ fontSize: '2rem', fontWeight: 'bold' }}>{title}</h1>
                <div className='flex flex-col gap-4'>
                    {cards.map((card) => (
                        <Card key={card.id} id={card.id} title={card.title}>{card.description}</Card>
                    ))}
                </div>
            </div>
        </SortableContext>
    );
};

export default Column;
