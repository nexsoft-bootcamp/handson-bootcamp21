import React from 'react'

export default function ButtonCustom({ name = "button", onCLick }) {
    return (
        <button className="btn btn-sm btn-primary" onClick={onCLick}>{name}</button>
    )
}
