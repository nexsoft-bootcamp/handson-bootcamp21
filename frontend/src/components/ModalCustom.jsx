import React from 'react'

export default function ModalCustom({ title, id, children, submitTitle, onClickSubmit }) {
    return (
        <dialog id={id} className="modal">
            <div className="modal-box w-4/12">
                <h3 className="font-bold text-lg">{title}</h3>
                <div className='py-8'>
                    {children}
                </div>
                <div className="modal-action">
                    <form method="dialog" className='flex gap-2'>
                        {
                            submitTitle ? <button type='submit' className="btn-sm btn btn-primary" onClick={onClickSubmit}>Create</button> : <></>
                        }
                        <button className="btn btn-sm">Close</button>
                    </form>
                </div>
            </div>
        </dialog>
    )
}
