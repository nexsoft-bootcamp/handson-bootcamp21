import React, { useState } from 'react';
import ModalCustom from './ModalCustom';
import { useNavigate } from 'react-router-dom';

export default function CreateTodo() {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [status, setStatus] = useState('');
    const navigate = useNavigate();

    const handleCreateTodo = () => {
        if (title && description) {
            const token = localStorage.getItem('token');
            if (!token) {
                alert('Token not found in localStorage');
                return;
            }

            fetch('http://localhost:8080/api/tasks', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({ title, description, status: "todo" }),
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Gagal menyimpan data');
                }
                alert("Tugas berhasil dibuat!");
                setTimeout(() => {
                    location.reload()
                }, 3000);
            })
            .catch(error => {
                console.error('Error:', error);
                alert("Terjadi kesalahan saat membuat tugas: " + error.message);
            });
        } else {
            alert("Harap isi judul dan deskripsi");
        }
    };

    const handleLogout = () => {
        localStorage.removeItem("token");
        navigate("/");
    };

    return (
        <div className="flex justify-between">
            <div>
                <button className="btn btn-primary" onClick={() => document.getElementById('1').showModal()}>Create To Do</button>
                {/* <button className="btn" onClick={() => document.getElementById('1').showModal()}>Create To Do</button> */}
            </div>
            <div>
                <button className="btn btn-neutral" onClick={handleLogout}>Logout</button>
            </div>
            <ModalCustom id={"1"} title={"Create Todo"} submitTitle={"Create"} onClickSubmit={handleCreateTodo}>
                <div className='flex flex-col gap-4'>
                    <label className="form-control w-full">
                        <div className="label">
                            <span className="label-text">Title</span>
                        </div>
                        <input type="text" placeholder="e.g memasak" className="input input-bordered input-md w-full" value={title} onChange={(e) => setTitle(e.target.value)} />
                    </label>
                    <label className="form-control w-full">
                        <div className="label">
                            <span className="label-text">Description</span>
                        </div>
                        <input type="text" placeholder="e.g let him cook" className="input input-bordered input-md w-full" value={description} onChange={(e) => setDescription(e.target.value)} />
                    </label>
                </div>
            </ModalCustom>
        </div>
    );
}
