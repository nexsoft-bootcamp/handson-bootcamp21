import { CSS } from "@dnd-kit/utilities";
import { useSensor, useSensors, PointerSensor, KeyboardSensor, DndContext, closestCorners } from '@dnd-kit/core';
import { arrayMove, sortableKeyboardCoordinates } from "@dnd-kit/sortable";
import Column from './components/Column';
import CreateTodo from './components/CreateTodo';
import Layout from './layout/Layout';
import { useState, useEffect } from 'react';

function App() {
  const data = [
    {
      id: "todo",
      title: "Todo",
      cards: [
        {
          id: "Card1",
          title: "Memasak",
          description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent pretium tempus nulla vel placerat. Aenean vestibulum ipsum ac enim ultrices dapibus. Sed maximus felis id venenatis facilisis.",
          status: "todo"
        },
        {
          id: "Card2",
          title: "Menyuciew",
          description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent pretium tempus nulla vel placerat. Aenean vestibulum ipsum ac enim ultrices dapibus. Sed maximus felis id venenatis facilisis.",
          status: "todo"
        }
      ]
    },
    {
      id: "doing",
      title: "Doing",
      cards: []
    },
    {
      id: "complete",
      title: "Complete",
      cards: []
    }
  ];

  const [columns, setColumns] = useState(data);

  useEffect(() => {
    const token = localStorage.getItem('token');
  
    if (token) {
      fetch('http://localhost:8080/api/tasks', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
        .then(response => {
          if (!response.ok) {
            throw new Error('Failed to fetch data');
          }
          return response.json();
        })
        .then(data => {
          const transformedData = transformData(data);
          setColumns(transformedData);
        })
        .catch(error => {
          console.error('Error fetching data:', error);
        });
    } else {
      console.error('Token not found in localStorage');
    }
  }, []);  

  const transformData = (data) => {
    const transformedData = [
      { id: "todo", title: "Todo", cards: [] },
      { id: "doing", title: "Doing", cards: [] },
      { id: "complete", title: "Complete", cards: [] }
    ];

    data.forEach(item => {
      const card = {
        id: item.id.toString() || Math.random(),
        title: item.title,
        description: item.description,
        status: "todo"
      };

      const columnIndex = item.status === 'doing' ? 1 : (item.status === 'complete' ? 2 : 0);

      transformedData[columnIndex].cards.push(card);
    });

    return transformedData;
  };

  const findColumn = (unique) => {
    if (!unique) {
      return null;
    }
    if (columns.some((c) => c.id === unique)) {
      return columns.find((c) => c.id === unique) ?? null;
    }
    const id = String(unique);
    const itemWithColumnId = columns.flatMap((c) => {
      const columnId = c.id;
      return c.cards.map((i) => ({ itemId: i.id, columnId: columnId }));
    });
    const columnId = itemWithColumnId.find((i) => i.itemId === id)?.columnId;
    return columns.find((c) => c.id === columnId) ?? null;
  };

  const handleDragOver = (event) => {
    const { active, over, delta } = event;
    const activeId = String(active.id);
    const overId = over ? String(over.id) : null;
    const activeColumn = findColumn(activeId);
    const overColumn = findColumn(overId);
    if (!activeColumn || !overColumn || activeColumn === overColumn) {
      return null;
    }
    setColumns((prevState) => {
      const activeItems = activeColumn.cards;
      const overItems = overColumn.cards;
      const activeIndex = activeItems.findIndex((i) => i.id === activeId);
      const overIndex = overItems.findIndex((i) => i.id === overId);
      const newIndex = () => {
        const putOnBelowLastItem =
          overIndex === overItems.length - 1 && delta.y > 0;
        const modifier = putOnBelowLastItem ? 1 : 0;
        return overIndex >= 0 ? overIndex + modifier : overItems.length + 1;
      };
      return prevState.map((c) => {
        if (c.id === activeColumn.id) {
          c.cards = activeItems.filter((i) => i.id !== activeId);
          return c;
        } else if (c.id === overColumn.id) {
          activeItems[activeIndex].status = c.id;
          c.cards = [
            ...overItems.slice(0, newIndex()),
            activeItems[activeIndex],
            ...overItems.slice(newIndex(), overItems.length)
          ];
          return c;
        } else {
          return c;
        }
      });
    });
  };

  const handleDragEnd = (event) => {
    const { active, over } = event;
    const activeId = String(active.id);
    const overId = over ? String(over.id) : null;

    const activeColumn = findColumn(activeId);
    const overColumn = findColumn(overId);

    if (!activeColumn || !overColumn || activeColumn !== overColumn) {
      return null;
    }

    const activeIndex = activeColumn.cards.findIndex((i) => i.id === activeId);
    const overIndex = overColumn.cards.findIndex((i) => i.id === overId);

    if (activeIndex !== overIndex) {
      setColumns((prevState) => {
        return prevState.map((column) => {
          if (column.id === activeColumn.id) {
            column.cards = arrayMove(overColumn.cards, activeIndex, overIndex);
            return column;
          } else {
            return column;
          }
        });
      });
    }
  };


  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates
    })
  );

  return (
    <Layout className={'py-6 min-w-lg'}>
      <CreateTodo />
      <DndContext
        sensors={sensors}
        collisionDetection={closestCorners}
        onDragEnd={handleDragEnd}
        onDragOver={handleDragOver}
      >
        <div
          className="mt-8 w-full grid grid-cols-3 gap-4 p-6 rounded min-h-96 bg-gray-700 overflow-auto"
          style={{ backgroundColor: '#B3A398' }}
        >
          {columns?.map((column) => (
            <Column
              key={column.id}
              id={column.id}
              title={column.title}
              cards={column.cards}
            ></Column>
          ))}
        </div>
      </DndContext>
    </Layout>
  )
}

export default App;
