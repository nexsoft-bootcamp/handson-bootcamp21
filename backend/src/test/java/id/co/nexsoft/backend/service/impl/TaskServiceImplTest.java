// package id.co.nexsoft.backend.service.impl;

// import static org.junit.jupiter.api.Assertions.assertSame;
// import static org.junit.jupiter.api.Assertions.assertTrue;
// import static org.mockito.Mockito.verify;
// import static org.mockito.Mockito.when;

// import id.co.nexsoft.backend.model.Task;
// import id.co.nexsoft.backend.repository.TaskRepository;

// import java.util.ArrayList;
// import java.util.List;
// import java.util.Optional;

// import org.junit.jupiter.api.Test;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.mockito.Mockito;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.mock.mockito.MockBean;
// import org.springframework.test.context.ContextConfiguration;
// import org.springframework.test.context.junit.jupiter.SpringExtension;

// @ContextConfiguration(classes = {TaskServiceImpl.class})
// @ExtendWith(SpringExtension.class)
// class TaskServiceImplTest {
//     @MockBean
//     private TaskRepository taskRepository;

//     @Autowired
//     private TaskServiceImpl taskServiceImpl;

//     /**
//      * Method under test: {@link TaskServiceImpl#getAllTasks()}
//      */
//     @Test
//     void testGetAllTasks() {
//         // Arrange
//         ArrayList<Task> taskList = new ArrayList<>();
//         when(taskRepository.findAll()).thenReturn(taskList);

//         // Act
//         List<Task> actualAllTasks = taskServiceImpl.getAllTasks();

//         // Assert
//         verify(taskRepository).findAll();
//         assertTrue(actualAllTasks.isEmpty());
//         assertSame(taskList, actualAllTasks);
//     }

//     /**
//      * Method under test: {@link TaskServiceImpl#getTaskById(Long)}
//      */
//     @Test
//     void testGetTaskById() {
//         // Arrange
//         Task task = new Task();
//         task.setDescription("The characteristics of someone or something");
//         task.setId(1L);
//         task.setStatus("Status");
//         task.setTitle("Dr");
//         Optional<Task> ofResult = Optional.of(task);
//         when(taskRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);

//         // Act
//         Task actualTaskById = taskServiceImpl.getTaskById(1L);

//         // Assert
//         verify(taskRepository).findById(Mockito.<Long>any());
//         assertSame(task, actualTaskById);
//     }

//     /**
//      * Method under test: {@link TaskServiceImpl#createTask(Task)}
//      */
//     @Test
//     void testCreateTask() {
//         // Arrange
//         Task task = new Task();
//         task.setDescription("The characteristics of someone or something");
//         task.setId(1L);
//         task.setStatus("Status");
//         task.setTitle("Dr");
//         when(taskRepository.save(Mockito.<Task>any())).thenReturn(task);

//         Task task2 = new Task();
//         task2.setDescription("The characteristics of someone or something");
//         task2.setId(1L);
//         task2.setStatus("Status");
//         task2.setTitle("Dr");

//         // Act
//         Task actualCreateTaskResult = taskServiceImpl.createTask(task2);

//         // Assert
//         verify(taskRepository).save(Mockito.<Task>any());
//         assertSame(task, actualCreateTaskResult);
//     }
// }
