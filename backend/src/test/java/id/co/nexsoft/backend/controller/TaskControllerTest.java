// package id.co.nexsoft.backend.controller;

// import static org.mockito.Mockito.when;

// import com.fasterxml.jackson.databind.ObjectMapper;
// import id.co.nexsoft.backend.model.Task;
// import id.co.nexsoft.backend.repository.TaskRepository;

// import java.util.ArrayList;
// import java.util.Optional;

// import org.junit.jupiter.api.Test;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.mockito.Mockito;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.mock.mockito.MockBean;
// import org.springframework.http.MediaType;
// import org.springframework.test.context.ContextConfiguration;
// import org.springframework.test.context.junit.jupiter.SpringExtension;
// import org.springframework.test.web.servlet.ResultActions;
// import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
// import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
// import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
// import org.springframework.test.web.servlet.setup.MockMvcBuilders;

// @ContextConfiguration(classes = {TaskController.class})
// @ExtendWith(SpringExtension.class)
// class TaskControllerTest {
//     @Autowired
//     private TaskController taskController;

//     @MockBean
//     private TaskRepository taskRepository;

//     /**
//      * Method under test: {@link TaskController#createTask(Task)}
//      */
//     @Test
//     void testCreateTask() throws Exception {
//         // Arrange
//         when(taskRepository.findAll()).thenReturn(new ArrayList<>());

//         Task task = new Task();
//         task.setDescription("The characteristics of someone or something");
//         task.setId(1L);
//         task.setStatus("Status");
//         task.setTitle("Dr");
//         String content = (new ObjectMapper()).writeValueAsString(task);
//         MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/tasks")
//                 .contentType(MediaType.APPLICATION_JSON)
//                 .content(content);

//         // Act and Assert
//         MockMvcBuilders.standaloneSetup(taskController)
//                 .build()
//                 .perform(requestBuilder)
//                 .andExpect(MockMvcResultMatchers.status().isOk())
//                 .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
//                 .andExpect(MockMvcResultMatchers.content().string("[]"));
//     }

//     /**
//      * Method under test: {@link TaskController#getAllTasks()}
//      */
//     @Test
//     void testGetAllTasks() throws Exception {
//         // Arrange
//         when(taskRepository.findAll()).thenReturn(new ArrayList<>());
//         MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/tasks");

//         // Act and Assert
//         MockMvcBuilders.standaloneSetup(taskController)
//                 .build()
//                 .perform(requestBuilder)
//                 .andExpect(MockMvcResultMatchers.status().isOk())
//                 .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
//                 .andExpect(MockMvcResultMatchers.content().string("[]"));
//     }

//     /**
//      * Method under test: {@link TaskController#getTaskById(Long)}
//      */
//     @Test
//     void testGetTaskById() throws Exception {
//         // Arrange
//         Task task = new Task();
//         task.setDescription("The characteristics of someone or something");
//         task.setId(1L);
//         task.setStatus("Status");
//         task.setTitle("Dr");
//         Optional<Task> ofResult = Optional.of(task);
//         when(taskRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
//         MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/tasks/{id}", 1L);

//         // Act and Assert
//         MockMvcBuilders.standaloneSetup(taskController)
//                 .build()
//                 .perform(requestBuilder)
//                 .andExpect(MockMvcResultMatchers.status().isOk())
//                 .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
//                 .andExpect(MockMvcResultMatchers.content()
//                         .string(
//                                 "{\"id\":1,\"title\":\"Dr\",\"description\":\"The characteristics of someone or something\",\"status\":\"Status\"}"));
//     }

//     /**
//      * Method under test: {@link TaskController#getTaskById(Long)}
//      */
//     @Test
//     void testGetTaskById2() throws Exception {
//         // Arrange
//         Optional<Task> emptyResult = Optional.empty();
//         when(taskRepository.findById(Mockito.<Long>any())).thenReturn(emptyResult);
//         MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/tasks/{id}", 1L);

//         // Act
//         ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(taskController).build().perform(requestBuilder);

//         // Assert
//         actualPerformResult.andExpect(MockMvcResultMatchers.status().isNotFound());
//     }
// }
