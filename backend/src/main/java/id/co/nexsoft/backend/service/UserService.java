package id.co.nexsoft.backend.service;

import java.util.List;

import id.co.nexsoft.backend.model.User;

public interface UserService {

    List<User> getAllUsers();

    User getUserById(Long id);

    String createUser(User user);

    String loginMessage(User user);
}

