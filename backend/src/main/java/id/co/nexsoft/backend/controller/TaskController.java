package id.co.nexsoft.backend.controller;

import id.co.nexsoft.backend.service.UserService;
import id.co.nexsoft.backend.service.impl.UserServiceImpl;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.co.nexsoft.backend.model.Task;
import id.co.nexsoft.backend.repository.TaskRepository;
import id.co.nexsoft.backend.model.User;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;

    @CrossOrigin(origins = "http://localhost:5173/")
    @GetMapping
    public List<Task> getAllTasks(@RequestHeader("Authorization") String token) {
        Object jwt = UserServiceImpl.getUserNameFromJwtToken(token.substring(7));
        JwtPayloadDto payload = mapToJwtPayloadDto(jwt);
        System.out.println(payload.getUserId());

        User user = new User();
        user.setId(Long.valueOf(payload.getUserId()));

        return taskRepository.findByUser(user);
    }

    private JwtPayloadDto mapToJwtPayloadDto(Object jwtPayloadObj) {
        if (jwtPayloadObj instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, Object> jwtPayloadMap = (Map<String, Object>) jwtPayloadObj;
            JwtPayloadDto jwtPayloadDto = new JwtPayloadDto();
            jwtPayloadDto.setUserId(String.valueOf((Integer) jwtPayloadMap.get("userId")));
            jwtPayloadDto.setIat(String.valueOf((Long) jwtPayloadMap.get("iat")));
            jwtPayloadDto.setSub((String) jwtPayloadMap.get("sub"));
            jwtPayloadDto.setExp(String.valueOf((Long) jwtPayloadMap.get("exp")));
            return jwtPayloadDto;
        }
        return null;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTaskById(@PathVariable Long id) {

        User user = new User();
        user.setId(id);

        List<Task> task = taskRepository.findByUser(user);
        return ResponseEntity.ok(task);
    }

    @CrossOrigin(origins = "http://localhost:5173/")
    @PostMapping
    public ResponseEntity<Task> createTask(@RequestBody TaskDto task, @RequestHeader("Authorization") String token) {
        Task newTask = new Task();

        Object jwt = UserServiceImpl.getUserNameFromJwtToken(token.substring(7));
        JwtPayloadDto payload = mapToJwtPayloadDto(jwt);

        User user = userService.getUserById(Long.valueOf(payload.getUserId()));

        newTask.setTitle(task.getTitle());
        newTask.setDescription(task.getDescription());
        newTask.setStatus("todo");
        newTask.setUser(user);

        Task savedTask = taskRepository.save(newTask);

        return ResponseEntity.status(HttpStatus.CREATED).body(savedTask);
    }

    @CrossOrigin(origins = "http://localhost:5173/")
    @GetMapping("/token")
    public Object getTokenData(@RequestHeader("Authorization") String token) {
        return UserServiceImpl.getUserNameFromJwtToken(token.substring(7));
    }
}

@Data
class TaskDto {
    private String title;
    private String description;
    private String status;
    private Long userId;
}

@Data
class JwtPayloadDto {
    private String userId;
    private String iat;
    private String sub;
    private String exp;
}
