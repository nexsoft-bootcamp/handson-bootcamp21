package id.co.nexsoft.backend.repository;

import id.co.nexsoft.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.model.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByUser(User user);

}

