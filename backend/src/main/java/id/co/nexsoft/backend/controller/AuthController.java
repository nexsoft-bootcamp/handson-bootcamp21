package id.co.nexsoft.backend.controller;

import id.co.nexsoft.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.co.nexsoft.backend.model.User;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AuthController {

    @Autowired
    private UserService userRepository;

    @GetMapping("/ping")
    public String getPing() {
        return "Hello ges";
    }

    @CrossOrigin(origins = "http://localhost:5173/")
    @GetMapping("/user")
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        User user = userRepository.getUserById(id);
        return ResponseEntity.ok(user);
    }

    @CrossOrigin(origins = "http://localhost:5173/")
    @PostMapping("/register")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        String savedUser = userRepository.createUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedUser);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login (@RequestBody User user) {
        String foundUser = userRepository.loginMessage(user);
        return ResponseEntity.ok(foundUser);
    }

}

