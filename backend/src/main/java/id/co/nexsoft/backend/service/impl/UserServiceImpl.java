package id.co.nexsoft.backend.service.impl;

import java.lang.reflect.Field;

import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.repository.UserRepository;
import id.co.nexsoft.backend.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public String createUser(User user) {
        User newUser = new User();
        User userFound = userRepository.findByEmail(user.getEmail());
        if (userFound == null) {
            newUser.setName(user.getName());
            newUser.setEmail(user.getEmail());
            newUser.setPassword(this.passwordEncoder.encode(user.getPassword()));
            userRepository.save(newUser);
            return newUser.getName();
        } else {
            return "Username Already Exist";
        }
    }

    @Override
    public String loginMessage(User user) {
        User foundUser = userRepository.findByEmail(user.getEmail());
        if (foundUser != null) {
            String password = user.getPassword();
            String encodedPassword = foundUser.getPassword();
            boolean isPwdRight = passwordEncoder.matches(password, encodedPassword);
            if (isPwdRight) {
                String jwt;
                jwt = createJWT(foundUser.getEmail(), foundUser.getId());
                return jwt;
            } else {
                return "Password not match";
            }
        }
        return "Username not Found";
    }

    private String createJWT(String username, Long userId) {
        String secretString = "2c70e12b7a0646f92279f427c7b38e7334d8e5389cff167a1dc30e73f826b683";

        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", userId);

        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretString));
        System.out.println(key);
        String jwt;
        jwt = Jwts.builder()
                .claims(claims)
                .subject(username)
                .issuedAt(new Date())
                .expiration(new Date(System.currentTimeMillis() + 86400000))
                .signWith(key)
                .compact();

        return jwt;
    }

    public static Object getUserNameFromJwtToken(String token) {
        String secretString = "2c70e12b7a0646f92279f427c7b38e7334d8e5389cff167a1dc30e73f826b683";
        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretString));
        Object jws = Jwts.parser()
                .verifyWith(key)
                .build()
                .parseSignedClaims(token).getPayload();

        return jws;

    }

    private static Map<String, Object> convertUsingReflection(Object object) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        Field[] fields = object.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(object));
        }

        return map;
    }
}

@Data
class JwtPayloadDto {
    private String userId;
    private String iat;
    private String sub;
    private String exp;
}