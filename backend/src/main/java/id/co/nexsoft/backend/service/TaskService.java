package id.co.nexsoft.backend.service;

import java.util.List;

import id.co.nexsoft.backend.model.Task;

public interface TaskService {

    List<Task> getAllTasks();

    Task getTaskById(Long id);

    Task createTask(Task task);
}

